<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $data = new User();
        $data->name = 'admin';
        $data->email = 'admin@gmail.com';
        $data->password = Hash::make('123456');
        $data->save();
    }
}
